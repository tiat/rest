<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Rest
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Rest\Controller;

//
use Tiat\Mvc\Controller\AbstractActionController;
use Tiat\Mvc\Helper\ControllerHelperTrait;

/**
 * An abstract class for RESTful controllers that extends the AbstractActionController class.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractRestController extends AbstractActionController implements RestControllerTraitInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use ControllerHelperTrait;
}
