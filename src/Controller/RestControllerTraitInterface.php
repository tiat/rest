<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Rest
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Rest\Controller;

//
use Tiat\Mvc\Helper\ControllerHelperTraitInterface;

/**
 * Interface RestControllerInterface
 * This interface represents a REST controller class that handles HTTP requests and responses.
 * It extends the ControllerHelperInterface to inherit the common controller functionality.
 * Implementing classes are responsible for implementing methods to handle different HTTP methods
 * such as GET, POST, PUT, DELETE, etc. These methods will be called by the framework depending on
 * the type of HTTP request received.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface RestControllerTraitInterface extends ControllerHelperTraitInterface {

}
