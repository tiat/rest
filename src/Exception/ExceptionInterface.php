<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Rest
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Rest\Exception;

//
use Throwable;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface ExceptionInterface extends Throwable {

}
